// const fs = require('fs')

const AWS = require('aws-sdk')

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

// const uploadLocalFile = (file, filePath) => {
//   return new Promise((resolve, reject) => {
//     let filename = file.hapi.filename
//     const fileType = file.hapi.filename.replace(/^.*\./, '')
//     const uniqueNum = new Date().getMilliseconds()
//     filename = uniqueNum + '_' + filename.replace(' ', '_')

//     const data = file._data
//     let imagePath = 'uploads/'
//     let path = './uploads'

//     if (!fs.existsSync(path)) {
//       fs.mkdirSync(path, { recursive: true })
//     }

//     if (!fs.existsSync(path)) {
//       fs.mkdirSync(path, { recursive: true })
//     }

//     if (filePath) {
//       path = `${path}/${filePath}`
//       imagePath = `${imagePath}${filePath}/`
//     }

//     if (!fs.existsSync(path)) {
//       fs.mkdirSync(path, { recursive: true })
//     }

//     fs.writeFile(`${path}/` + filename, data, err => {
//       if (err) {
//         reject(err)
//       }
//       resolve({
//         message: 'Uploaded successfully!',
//         success: true,
//         filePath: `${imagePath}${filename}`,
//         fileName: filename,
//         fileType: fileType
//       })
//     })
//   })
// }

const handleFileUpload = (file, filePath = null) => {
  // return uploadLocalFile(file, filePath)
  return uploadFileToBucket(file, filePath)
}

const uploadFileToBucket = (file, filePath) => {
  return new Promise((resolve, reject) => {
    const data = file._data
    let filename = file.hapi.filename
    const fileType = filename.replace(/^.*\./, '')
    const uniqueNum = new Date().getMilliseconds()
    filename = uniqueNum + '_' + filename.replace(' ', '_')
    if (!filePath) {
      filePath = 'profile'
    }
    const params = {
      Bucket: 'ethicalcode-assets', // pass your bucket name
      Key: `${filePath}/${filename}`, // file will be saved as testBucket/contacts.csv
      Body: data
    }
    s3.upload(params, (s3Err, data) => {
      if (s3Err) {
        throw s3Err
      }
      console.log(`File uploaded successfully at ${data.Location}`)
      resolve({
        message: 'Uploaded successfully!',
        success: true,
        filePath: `${filePath}/${filename}`,
        fileName: filename,
        fileType: fileType
      })
    })
  })
}

const deleteFile = filePath => {
  return new Promise((resolve, reject) => {
    var params = {
      Bucket: 'ethicalcode-assets', // pass your bucket name
      Key: `${filePath}` // file will be saved as testBucket/contacts.csv
    }
    s3.deleteObject(params, (err, data) => {
      if (data) {
        resolve({
          message: 'Deleted successfully!',
          status: true
        })
      } else {
        resolve({
          message: 'Check if you have sufficient permissions!',
          status: false
        })
        console.log('Check if you have sufficient permissions : ' + err)
      }
    })
  })
}

// const deleteFileOld = async file => {
//   return new Promise((resolve, reject) => {
//     fs.stat('./' + file, (err, exists) => {
//       if (exists) {
//         fs.unlinkSync('./' + file)
//         resolve({
//           message: 'Deleted successfully!'
//         })
//       } else {
//         if (err) {
//           reject(err)
//         }
//       }
//     })
//   })
// }

const uploadNewsFileToBucket = (file, filePath, filename) => {
  return new Promise((resolve, reject) => {
    const params = {
      Bucket: 'ethicalcode-assets', // pass your bucket name
      Key: `${filePath}/${filename}`, // file will be saved as testBucket/contacts.csv
      Body: file
    }
    s3.upload(params, (s3Err, data) => {
      if (s3Err) {
        throw s3Err
      }
      resolve({
        message: 'Uploaded successfully!',
        success: true,
        filePath: `${filePath}/${filename}`,
        fileName: filename
      })
    })
  })
}

const getFileObject = async _keys => {
  return await Promise.all(
    _keys.map(
      _key =>
        new Promise((resolve, reject) => {
          s3.getObject({ Bucket: 'ethicalcode-assets', Key: _key }, function(
            err,
            _data
          ) {
            if (err) {
              resolve({ success: false })
            }
            resolve({
              success: true,
              data: _data.Body,
              name: `${_key.split('/').pop()}`
            })
          })
        })
    )
  ).catch(_err => {
    throw new Error(_err)
  })
}

const uploadZipFile = _key => {
  const stream = require('stream')
  const _pass = new stream.PassThrough()
  s3.upload(
    {
      Bucket: 'ethicalcode-assets',
      Key: _key,
      Body: _pass
    },
    (err, data) => {
      console.log('err: ', err)
      console.log('data: ', data)
    }
  )
  return _pass
}

module.exports = {
  handleFileUpload,
  deleteFile,
  uploadNewsFileToBucket,
  getFileObject,
  uploadZipFile
}
