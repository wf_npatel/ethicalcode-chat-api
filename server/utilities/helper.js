'use strict'

const Joi = require('@hapi/joi')
Joi.objectId = Joi.string
const Boom = require('@hapi/boom')
const config = require('config')
const errorHelper = require('@utilities/error-helper')
const moment = require('moment-timezone')

const apiHeaders = () => {
  return Joi.object({
    authorization: Joi.string()
  }).options({
    allowUnknown: true
  })
}

const encodeBase64 = string => {
  return Buffer.from(string).toString('base64')
}

const decodeBase64 = string => {
  return Buffer.from(string, 'base64').toString()
}

const sendEmail = async mailObj => {
  let response
  try {
    response = await mailGunSendMail(mailObj).catch(e => console.log(e))
    return response
  } catch (err) {
    errorHelper.handleError(err)
  }
}

const mailGunSendMail = mailObj => {
  return new Promise((resolve, reject) => {
    var mailGun = require('mailgun-js')({
      apiKey: config.constants.MAIL_GUN_API_KEY,
      domain: config.constants.MAIL_GUN_DOMAIN
    })

    if (mailObj) {
      const messageObj = {}
      if (mailObj.message) {
        messageObj.text = mailObj.message
        delete mailObj.message
      } else {
        messageObj.html = mailObj.html
        delete mailObj.html
      }

      var data = {
        from: config.constants.MAIL_FROM,
        ...mailObj,
        ...messageObj
      }

      try {
        mailGun.messages().send(data, function(error, body) {
          if (error) {
            reject(error)
          }
          resolve(body.message)
        })
      } catch (err) {
        errorHelper.handleError(err)
      }
    } else {
      errorHelper.handleError(Boom.badData('Mail Detail is invalid'))
    }
  })
}

const getAccessLevel = async (accessLevelID, type) => {
  const AccessLevel = require('@models/accesslevel.model').schema
  const query = {}
  query.accessType = type
  query[type] = accessLevelID
  query.isAccepted = true
  query.isBlocked = false
  query.isRejected = false
  query.isDeleted = false

  const response = await AccessLevel.find(query)
  return response.map(a => a.invitedBy)
}

const userBlockedFor = async (userId, type) => {
  const AccessLevel = require('@models/accesslevel.model').schema
  const response = await AccessLevel.find({
    accessType: type,
    isBlocked: true,
    $or: [
      {
        invitedTo: userId
      },
      {
        blockedUser: userId
      }
    ]
  })
  return response.map(a => a[type])
}

const sourceSort = async data => {
  const sortedNameData = data.sort((a, b) => {
    return a.name.toLowerCase().localeCompare(b.name.toLowerCase())
  })
  const sortedFileFolderData = sortedNameData.sort((a, b) => {
    return a.isFile - b.isFile
  })
  return sortedFileFolderData
}

const generateRandomString = length => {
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  let result = ''
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

const currentYear = () => {
  return {
    from: moment(moment.utc())
      .startOf('year')
      .toISOString(),
    to: moment(moment.utc())
      .endOf('year')
      .toISOString()
  }
}

const currentYearMonthNames = () => {
  const currentFullYear = new Date().getFullYear().toString()
  const dateArray = [
    '',
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
    ''
  ]
  return dateArray.map(data => `${data}${currentFullYear}`)
}

const makeTimeSlots = (from, to) => {
  const options = []
  for (let index = from; index <= to; index++) {
    if (index < to) {
      options.push({
        from: index,
        to: index + 0.5
      })
    }
    if (index + 0.5 < to) {
      options.push({
        from: index + 0.5,
        to: index + 1
      })
    }
  }
  return options
}

// key, array
const inArray = (needle, haystack) => {
  var length = haystack.length
  for (var i = 0; i < length; i++) {
    if (haystack[i] === needle) return true
  }
  return false
}

module.exports = {
  apiHeaders,
  sendEmail,
  encodeBase64,
  decodeBase64,
  getAccessLevel,
  userBlockedFor,
  sourceSort,
  generateRandomString,
  currentYear,
  currentYearMonthNames,
  makeTimeSlots,
  inArray
}
