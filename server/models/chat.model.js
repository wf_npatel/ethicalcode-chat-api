'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const modelName = 'chat'

const aiDBConn = require('@plugins/mongoose.plugin').plugin.aiDBConn()

const chatSchema = new Schema(
  {
    conversation: {
      type: Types.ObjectId,
      ref: 'conversation',
      require: true
    },
    user: {
      type: Types.ObjectId,
      ref: 'user',
      require: true
    },
    text: {
      type: Types.String,
      require: true
    },
    attechment: {
      type: Types.String,
      default: null
    },
    createdAt: {
      type: Types.Date,
      default: null
    },
    updatedAt: {
      type: Types.Date,
      default: null
    }
  },
  {
    versionKey: false,
    strict: false,
    timestamps: true
  }
)

exports.schema = aiDBConn.model(modelName, chatSchema)
