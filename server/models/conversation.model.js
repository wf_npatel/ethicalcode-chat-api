'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const modelName = 'conversation'

const aiDBConn = require('@plugins/mongoose.plugin').plugin.aiDBConn()

const conversationSchema = new Schema(
  {
    chat: {
      type: Types.ObjectId,
      ref: 'chat',
      require: true
    },
    users: {
      type: Types.Mixed,
      default: null
    },
    // usersHasNewMessages: {
    //   type: Types.Mixed,
    //   default: null
    // },
    isGroup: {
      type: Types.Boolean,
      default: false
    },
    createdAt: {
      type: Types.Date,
      default: null
    },
    updatedAt: {
      type: Types.Date,
      default: null
    }
  },
  {
    versionKey: false,
    strict: false,
    timestamps: true
  }
)

exports.schema = aiDBConn.model(modelName, conversationSchema)
