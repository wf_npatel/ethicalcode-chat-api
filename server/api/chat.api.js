'use strict'

const Joi = require('@hapi/joi')

Joi.objectId = Joi.string
const errorHelper = require('@utilities/error-helper')
const generalHelper = require('@utilities/helper')
const Chat = require('@models/chat.model').schema
let users = {};

module.exports = {
  getAll: {
    validate: {
    },
    pre: [],
    connect: ({ ctx, ws, wss }) => {

      const sendTo = (connection, message) => {
        console.log('connection, message: ', connection, message);
        connection.send(JSON.stringify(message));
      };

      const sendToAll = (clients, type, { id, name: userName }) => {
        console.log('sendToAll: ', clients, type, { id, name: userName });
        Object.values(clients).forEach(client => {
          if (client.name !== userName) {
            client.send(
              JSON.stringify({
                type,
                user: { id, userName }
              })
            )
          }
        })
      };

      console.log('cccccccccccccccccccc');

      ws.on("message", msg => {
        console.log("Received message: %s", msg);
        let data;

        //accepting only JSON messages
        try {
          data = JSON.parse(msg);
        } catch (e) {
          console.log("Invalid JSON");
          data = {};
        }
        const { type, name, offer, answer, candidate } = data;
        switch (type) {
          //when a user tries to login
          case "login":
            //Check if username is available
            if (users[name]) {
              sendTo(ws, {
                type: "login",
                success: false,
                message: "Username is unavailable"
              });
            } else {
              const uuidv4 = require("uuid/v4");
              const id = uuidv4();
              const loggedIn = Object.values(
                users
              ).map(({ id, name: userName }) => ({ id, userName }));
              // const loggedIn = Object.keys(users).map(user => ({ userName: user }));
              users[name] = ws;
              console.log('users: ', users);
              ws.name = name;
              ws.id = id;
              console.log('ws: ', ws);

              sendTo(ws, {
                type: "login",
                success: true,
                users: loggedIn
              });
              sendToAll(users, "updateUsers", ws);
            }
            break;
          case "offer":
            //if UserBexists then send him offer details
            const offerRecipient = users[name];

            if (!!offerRecipient) {
              //setting that sender connected with cecipient
              ws.otherName = name;
              sendTo(offerRecipient, {
                type: "offer",
                offer,
                name: ws.name
              });
            }
            break;
          case "answer":
            //for ex. UserB answers UserA
            const answerRecipient = users[name];

            if (!!answerRecipient) {
              ws.otherName = name;
              sendTo(answerRecipient, {
                type: "answer",
                answer
              });
            }
            break;
          case "candidate":
            const candidateRecipient = users[name];

            if (!!candidateRecipient) {
              sendTo(candidateRecipient, {
                type: "candidate",
                candidate
              });
            }
            break;
          case "leave":
            recipient = users[name];

            //notify the other user so he can disconnect his peer connection
            if (!!recipient) {
              recipient.otherName = null;
              sendTo(recipient, {
                type: "leave"
              });
            }
            break;
          default:
            sendTo(ws, {
              type: "error",
              message: "Command not found: " + type
            });
            break;
        }
      });

      ws.on("close", function () {
        if (ws.name) {
          delete users[ws.name];
          if (ws.otherName) {
            console.log("Disconnecting from ", ws.otherName);
            const recipient = users[ws.otherName];
            if (!!recipient) {
              recipient.otherName = null;
            }
          }
          sendToAll(users, "removeUser", ws);
        }
      });
      //send immediatly a feedback to the incoming connection


      console.log('ccomees');
      ws.send(
        JSON.stringify({
          type: "connect",
          message: "Well hello there, I am a WebSocket server"
        })
      );
    },
    disconnect: ({ ctx }) => {
      if (ctx.to !== null) {
        ctx.to = null
      }
    },
    handler: async (request, h) => {
      try {
        let { mode } = request.websocket()
        // console.log('request.websocket(): ', request.websocket());
        // console.log('mode: ', mode);
        return { at: "bar", mode: mode, seen: request.payload }
      } catch (e) {
        errorHelper.handleError(e)
      }
    }
  }
}
