'use strict'
// Never take constants here
module.exports = {
  plugin: {
    async register(server, options) {
      const API = require('@api/chat.api');
      server.route([
        {
          method: 'GET',
          path: '/chats',
          config: {
            plugins: {
            },
            tags: ['api', 'Chat'],
            description: 'Get Chat',
            notes: 'Get Chat',
            validate: API.getAll.validate,
            pre: API.getAll.pre,
            handler: API.getAll.handler
          }
        },
        {
          method: 'POST',
          path: '/bar',
          config: {
            plugins: {
              websocket: {
                only: true,
                initially: true,
                connect: API.getAll.connect,
                disconnect: API.getAll.disconnect
              }
            },
            tags: ['api', 'Chat'],
            description: 'Get Chat',
            notes: 'Get Chat',
            validate: API.getAll.validate,
            pre: API.getAll.pre,
            handler: API.getAll.handler
          }
        },
      ])
    },
    version: require('../../package.json').version,
    name: 'chat-routes'
  }
}
