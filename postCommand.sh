# this file execute after git pull code will complete
# $1 you will get current path on server a
#!/bin/bash
echo "post command"
echo "path : $1"
cd "$1"
# npm install
rm /var/www/html/demo/vai/api/logs/log.txt
rm /var/www/html/demo/vai/api/logs/error.txt
rm /var/www/html/demo/vai/api/logs/output.txt
forever stop vai
sudo kill `sudo lsof -t -i:8007`
forever start -c "npm run start:prod" -l "/var/www/html/demo/vai/api/logs/log.txt" -e "/var/www/html/demo/vai/api/logs/error.txt" -o "/var/www/html/demo/vai/api/logs/output.txt" --uid "vai" -a ./