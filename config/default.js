module.exports = {
  appName: 'VIRTUOUS-AI-API',
  console_url: 'http://localhost:4205',

  port: 1338,

  debug: {
    request: ['error', 'info'],
    log: ['info', 'error', 'warning']
  },

  constants: {
    API_BASEPATH: 'localhost:1338',
    EXPIRATION_PERIOD: '730d',
    VERIFICATION_EXPIRATION_PERIOD: '24',
    JWT_SECRET: 'virtuousaiapi',
    MAIL_FROM: 'demo1.webfirminfotech@gmail.com',
    MAIL_GUN_API_KEY: 'key-de4582feecd40bce619439a152ce7d23',
    MAIL_GUN_DOMAIN: 'mailing.webfirminfotech.com',
    GIT_ENDPOINT: 'http://165.22.16.26:81',
    GIT_DOMAIN: 'git.aakba.com',
    GIT_HTTPS: 'https'
    // GIT_ENDPOINT: "http://165.22.16.26:81"
  },

  git: {
    PASSWORD: 'admin@7941#'
  },

  connections: {
    db: process.env.DB
  }
}
