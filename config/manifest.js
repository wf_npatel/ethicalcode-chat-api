'use strict'

// const DEVELOPMENT = 'development'
const PRODUCTION = 'production'
const getArgument = argument => {
  return process.argv.indexOf(argument)
}

if (getArgument('--development') !== -1) {
  process.env.NODE_ENV = 'development'
}

if (getArgument('--prod') !== -1) {
  process.env.NODE_ENV = 'production'
}

if (getArgument('--development') !== -1 || getArgument('--prod') !== -1) {
  process.env.NODE_CONFIG_DIR = `${__dirname}`
}

const config = require('config')
const mongoose = require('mongoose')
const Config = JSON.parse(JSON.stringify(config))
const HAPIWebSocket = require("hapi-plugin-websocket")

// REF: https://github.com/glennjones/hapi-swagger/blob/master/optionsreference.md
const swaggerOptions = {
  info: {
    title: 'VIRTUOUS-AI',
    version: require('../package.json').version
  },
  documentationPath: '/docs',
  basePath: '/api/v1',
  tags: [],
  grouping: 'tags',
  securityDefinitions: {
    jwt: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header'
    }
  },
  security: [
    {
      Basic: []
    }
  ]
}

const DEFAULT = 'default'

let plugins = []
const ENV = config.util.getEnv('NODE_ENV').trim()
if (ENV !== DEFAULT) {
  swaggerOptions.schemes = ['https', 'http']
  // swaggerOptions.host = 'vai-api.aakba.com'
  swaggerOptions.host = Config.constants.API_BASEPATH
  mongoose.set('debug', true)
}
// if (ENV !== PRODUCTION) {
plugins = [
  {
    plugin: '@hapi/inert'
  },
  {
    plugin: '@hapi/vision'
  },
  {
    plugin: 'hapi-swagger',
    options: swaggerOptions
  },
  {
    plugin: 'hapi-dev-errors',
    options: {
      showErrors: process.env.NODE_ENV !== 'production',
      toTerminal: true
    }
  }
]
// }
plugins = plugins.concat([
  {
    plugin: '@hapi/good',
    options: {
      ops: {
        interval: 1000
      },
      reporters: {
        myConsoleReporter: [
          {
            module: '@hapi/good-squeeze',
            name: 'Squeeze',
            args: [
              {
                log: '*',
                request: '*',
                response: '*',
                error: '*'
              }
            ]
          },
          {
            module: '@hapi/good-console'
          },
          'stdout'
        ]
      }
    }
  },
  {
    plugin: 'hapi-auth-jwt2'
  },
  {
    plugin: '@hapi/basic'
  },
  {
    plugin: 'schmervice'
  },
  {
    plugin: 'mrhorse',
    options: {
      policyDirectory: `${__dirname}/../server/policies`,
      defaultApplyPoint:
        'onPreHandler' /* optional.  Defaults to onPreHandler */
    }
  },
  {
    plugin: '@plugins/mongoose.plugin',
    options: {
      connections: Config.connections
    }
  },
  {
    plugin: '@routes/root.route'
  },
  {
    plugin: HAPIWebSocket,
    options: {
      create: (wss) => { }
    }
  }
])

const routesOb = {
  'chat.route': '',
}

const routes = Object.keys(routesOb)

routes.forEach(r => {
  plugins = plugins.concat([
    {
      plugin: `@routes/${r}`,
      routes: {
        prefix: `/api/v1`
      }
    }
  ])
})

exports.manifest = {
  server: {
    router: {
      stripTrailingSlash: true,
      isCaseSensitive: false
    },
    routes: {
      security: {
        hsts: false,
        xss: true,
        noOpen: true,
        noSniff: true,
        xframe: false
      },
      cors: {
        origin: ['*'],
        // ref: https://github.com/hapijs/hapi/issues/2986
        headers: ['Accept', 'Authorization', 'Content-Type']
      },
      validate: {
        failAction: async (request, h, err) => {
          request.server.log(
            ['validation', 'error'],
            'Joi throw validation error'
          )
          throw err
        }
      },
      jsonp: 'callback', // <3 Hapi,
      auth: false // remove this to enable authentication or set your authentication profile ie. auth: 'jwt'
    },
    debug: Config.debug,
    port: Config.port
  },
  register: {
    plugins
  }
}

exports.options = {}
