module.exports = {
  appName: 'VIRTUOUS-AI-API',
  console_url: 'https://cloud.virtuousai.com',
  constants: {
    API_BASEPATH: 'cloud-api.virtuousai.com',
    EXPIRATION_PERIOD: '730d',
    VERIFICATION_EXPIRATION_PERIOD: '24',
    JWT_SECRET: 'virtuousaiapi',
    MAIL_FROM: 'demo1.webfirminfotech@gmail.com',
    MAIL_GUN_API_KEY: 'key-de4582feecd40bce619439a152ce7d23',
    MAIL_GUN_DOMAIN: 'mailing.webfirminfotech.com'
  },

  port: 1338,
  debug: {
    request: ['error', 'info'],
    log: ['info', 'error', 'warning']
  },
  connections: {
    db: process.env.DB
  }
}
