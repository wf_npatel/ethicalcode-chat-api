module.exports = {
  appName: 'VIRTUOUS-AI-API',
  port: 1338,
  console_url: 'https://vai.aakba.com',
  constants: {
    API_BASEPATH: 'ec-api.aakba.com',
    EXPIRATION_PERIOD: '730d',
    VERIFICATION_EXPIRATION_PERIOD: '24',
    JWT_SECRET: 'virtuousaiapi',
    MAIL_FROM: 'demo1.webfirminfotech@gmail.com',
    MAIL_GUN_API_KEY: 'key-de4582feecd40bce619439a152ce7d23',
    MAIL_GUN_DOMAIN: 'mailing.webfirminfotech.com'
  },

  debug: {
    request: ['error', 'info'],
    log: ['info', 'error', 'warning']
  },
  connections: {
    db: process.env.DB
  }
}
